import { Component, OnInit } from '@angular/core';
import { AutenticacionPage } from '../autenticacion/autenticacion.page';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';

import { UsuarioI } from '../../entities/usuario.interface';
import { SolicitudI } from '../../entities/solicitud.interface';
import { RepositorioUsuarioService } from '../../services/repositorio-usuario.service';
import { RepositorioSolicitudService } from '../../services/repositorio-solicitud.service';
import { AlertController } from '@ionic/angular';
import { Observable } from 'rxjs';


@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.page.html',
  styleUrls: ['./inicio.page.scss'],
})
export class InicioPage implements OnInit {

  usuarios: UsuarioI[];
  usuario: UsuarioI;
  usuarioNuevo: UsuarioI = {
    'fullName': 'Karolita',
    'identification': '123456',
    'phoneNumber': '+3013456612'
  };
  usuarioNuevo2: UsuarioI = {
    'fullName': 'Nuevo user',
    'identification': '78910',
    'phoneNumber': '+3013456612'
  };

  solicitud: SolicitudI = {
		'additionalImageURL': 'Prueba3 se convierte en la 4',
		'additionalInfo': 'Prueba3',
		'address': 'Prueba3',
		'emergencyType': 'Prueba3',
		'latitude': 75.0005,
		'longitude': -78.555,
		'userId': 'xX9rgacedMa8Pd6pLzUp'
  };

  constructor(
    public popoverController: PopoverController, 
    private repositorioUsuarioService: RepositorioUsuarioService,
    private repositorioSolicitudService: RepositorioSolicitudService,
    private alertController: AlertController) { }

  ngOnInit() {
  }

  openPopover(ev: Event) {
    this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    }).then((popover)=>{
        popover.present();
    }).catch((error)=>{
        console.log('Error abriendo el popover');
    });
  }

}
