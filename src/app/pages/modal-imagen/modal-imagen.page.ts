import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-imagen',
  templateUrl: './modal-imagen.page.html',
  styleUrls: ['./modal-imagen.page.scss'],
})
export class ModalImagenPage implements OnInit {

  @ViewChild('slider', { read: ElementRef })slider: ElementRef;
  img: any;
 
  sliderOpts = {
    ipagination: '.swiper-pagination',
    slidesPerView: 1,
    spaceBetween: 30
  };
 
  constructor(private navParams: NavParams, private modalController: ModalController) { }
 
  ngOnInit() {
  }
 
  close() {
    this.modalController.dismiss();
  }

}
