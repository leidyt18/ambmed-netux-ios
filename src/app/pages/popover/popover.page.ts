import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { SesionService } from '../../services/sesion.service';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { ModalImagenPage } from '../modal-imagen/modal-imagen.page';

@Component({
  selector: 'app-popover',
  templateUrl: './popover.page.html',
  styleUrls: ['./popover.page.scss'],
})
export class PopoverPage implements OnInit {

  constructor(
    private popOverConstroller : PopoverController,
    public sesionService: SesionService,
    private router: Router,
    private modalController: ModalController) { }

  ngOnInit() {
  }

  closePopover(){
    this.popOverConstroller.dismiss();
  }

  mostrarAcercaDe(){
    console.log('Click en Acerca de...');
    this.modalController.create({
      component: ModalImagenPage
    }).then(modal => {
      modal.present();
    });
    this.closePopover();
  }

  salir(){
    console.log('Click en Salir');
    this.closePopover();
    this.sesionService.cerrarSesion();
  }

}
