import { Component, OnInit } from '@angular/core';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { NavController } from '@ionic/angular';
import { ActivatedRoute } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { SesionService } from '../../services/sesion.service';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';

@Component({
  selector: 'app-autorizacion',
  templateUrl: './autorizacion.page.html',
  styleUrls: ['./autorizacion.page.scss'],
})
export class AutorizacionPage implements OnInit {

  codigo : string;
  idVerificacion : string = null;
  loadingRef = null;

  constructor(
    public navCtrl: NavController, 
    public firebaseAuthentication: FirebaseAuthentication,
    private activatedRoute: ActivatedRoute,
    private alertController: AlertController,
    public sesionService: SesionService,
    public popoverController: PopoverController,
    private router: Router,
    private loadingController: LoadingController, 
  ) { }

  ngOnInit() {
    this.idVerificacion = this.activatedRoute.snapshot.paramMap.get('idVerificacion');
  }

  validar(){
    this.mostrarLoading();
    this.firebaseAuthentication.signInWithVerificationId(this.idVerificacion, this.codigo).then((user)=>{
      this.finalizarLoading();
      console.log('Usuario SignIn: ', user);
      this.router.navigateByUrl('/registro');
    }).catch((error)=>{
      this.finalizarLoading();
      this.mostrarAlerta('Error', 'El código de verificación usado es inválido.');
    });
  }

  mostrarLoading(){
    this.loadingController.create({
      message: 'Verificando...'
    }).then((loading=>{
      this.loadingRef = loading;
      this.loadingRef.present();
    }));
  }

  finalizarLoading(){
    this.loadingRef.dismiss();
  }

  async mostrarAlerta(titulo: string, mensaje: string) {
    const alert = await this.alertController.create({
      header: titulo,
      message: mensaje,
      buttons: ['ACEPTAR']
    });

    await alert.present();
  }

  openPopover(ev: Event) {
    this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    }).then((popover)=>{
        popover.present();
    }).catch((error)=>{
        console.log('Error abriendo el popover');
    });
  }

}
