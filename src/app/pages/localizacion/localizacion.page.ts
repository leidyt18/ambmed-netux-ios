import { Component, OnInit } from '@angular/core';
import { LoadingController } from '@ionic/angular';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { Platform, NavController } from "@ionic/angular";
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';
import { RepositorioSolicitudService } from '../../services/repositorio-solicitud.service';

import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  LatLng,
  Geocoder,
  GeocoderRequest,
  GeocoderResult,
  HtmlInfoWindow
} from "@ionic-native/google-maps";


@Component({
  selector: 'app-localizacion',
  templateUrl: './localizacion.page.html',
  styleUrls: ['./localizacion.page.scss'],
})
export class LocalizacionPage implements OnInit {

  mapRef = null;
  loadingRef = null;
  elementoMapa: HTMLElement;
  elementoBtnSolicitarAmb: HTMLElement;
  elementoLblDireccion: HTMLElement;
  coordenadas: LatLng = null;
  map: GoogleMap;
  direccion: string = "";
  geocoder: Geocoder;
  infowindow: HtmlInfoWindow;
  geocoderRequest: GeocoderRequest;

  constructor(
    public plt: Platform, 
    public nav: NavController, 
    public geolocation: Geolocation, 
    public loadingController: LoadingController,
    private router: Router,
    public popoverController: PopoverController,
    private repositorioSolicitudService: RepositorioSolicitudService
  ) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.plt.ready().then(() => {
      this.cargarMapa();
    });
  }

  cargarMapa() {
    this.mostrarLoading('Cargando...');
    this.elementoMapa = document.getElementById('map');
    this.elementoBtnSolicitarAmb = document.getElementById('btnSolicitarAmb');
    this.elementoLblDireccion = document.getElementById('lblDireccion');
    this.obtenerLatitudyLongitud();
  }

  obtenerLatitudyLongitud(){
    this.geolocation.getCurrentPosition().then(respuestaPosicion => {
      if(respuestaPosicion && respuestaPosicion.coords && respuestaPosicion.coords.latitude && respuestaPosicion.coords.longitude){
        this.coordenadas = new LatLng(respuestaPosicion.coords.latitude, respuestaPosicion.coords.longitude);
        this.pintarMapa();
      }else{
        console.log('La respuesta de geolocation.getCurrentPosition() es inválida.');
      }
    }).catch(err => {
      console.log('Error obteniendo las coordenadas en geolocation.getCurrentPosition(): ', err);
    });
  }

  //Se crea el mapa y se le agrega el marker de punto de ubicación.
  pintarMapa(){
    this.map = GoogleMaps.create(this.elementoMapa);

    this.map.one(GoogleMapsEvent.MAP_READY).then((data: any) => {
      let position = {
        target: this.coordenadas,
        zoom: 15
      };

      this.map.moveCamera(position);
      this.map.setMyLocationEnabled(true);
      this.map.setMyLocationButtonEnabled(true);
    });
    this.registrarEventosDelMapa();
  }

  registrarEventosDelMapa(){
    this.map.on(GoogleMapsEvent.CAMERA_MOVE).subscribe((params:any[]) => {

      let depto: string;
      let dir: string;
      let municipio: string;
      let cameraTarget = this.map.getCameraTarget();

      Geocoder.geocode({"position": cameraTarget}).then((results: GeocoderResult[]) => {
        if (results.length == 0) {
          return null;
        }
        let address: any = [
          results[0].thoroughfare || "",
          results[0].locality || "",
          results[0].adminArea || "",
          results[0].country || ""];

        depto = results[0].adminArea;
        municipio = results[0].locality
        dir = results[0].thoroughfare;

        this.direccion = address.join(", ");

        if(depto === 'Antioquia' && dir !== '(null)' && municipio !== '(null)' && dir !== 'Unnamed Road'){
          this.elementoLblDireccion.innerText = dir + ', ' + municipio + ', ' + depto;
          this.elementoBtnSolicitarAmb.setAttribute('disabled', 'false');
         }else{
          this.elementoLblDireccion.innerText = 'Ésta dirección no es válida para la solicitud.';
          this.elementoBtnSolicitarAmb.setAttribute('disabled', 'true');
        }
      });
    });
  }

  solicitarAmbulancia(){
    let cameraTarget = this.map.getCameraTarget();
    if(this.repositorioSolicitudService.solicitudAmb){
      this.repositorioSolicitudService.solicitudAmb.latitude = cameraTarget.lat;
      this.repositorioSolicitudService.solicitudAmb.longitude = cameraTarget.lng;
      this.repositorioSolicitudService.solicitudAmb.address = this.elementoLblDireccion.innerText;
    }else{
      console.log('repositorioSolicitudService.solicitudAmb no esta definido');
    }
    this.router.navigateByUrl('/confirmacion');
  }

  mostrarLoading(mensaje: string){
    this.loadingController.create({
      message: mensaje
    }).then(loading => {
      loading.present();
      setTimeout(() => {
        loading.dismiss();
      }, 3000);
    });
  }

  openPopover(ev: Event) {
    this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    }).then((popover)=>{
        popover.present();
    }).catch((error)=>{
        console.log('Error abriendo el popover');
    });
  }

}
