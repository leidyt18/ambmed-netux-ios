import { Component, OnInit } from '@angular/core';
import { compileNgModuleFactory__POST_R3__ } from '@angular/core/src/application_ref';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { NavController } from '@ionic/angular';
import { LoadingController } from '@ionic/angular';
import { Router } from '@angular/router';
import { AlertController } from '@ionic/angular';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';

@Component({
  selector: 'app-autenticacion',
  templateUrl: './autenticacion.page.html',
  styleUrls: ['./autenticacion.page.scss'],
})
export class AutenticacionPage implements OnInit {

  numeroCelular: number;
  idVerificacion: string;
  loadingRef = null;

  constructor(
    private loadingController: LoadingController, 
    public navCtrl: NavController, private router: Router,
    public firebaseAuthentication: FirebaseAuthentication,
    private alertController: AlertController,
    public popoverController: PopoverController
  ) { }

  ngOnInit() {

  }

  autenticarUsuario(){

    this.mostrarLoading();
   
    this.firebaseAuthentication.verifyPhoneNumber("+57"+this.numeroCelular, 30000).then((verificationID) => {

      this.finalizarLoading();
      this.idVerificacion = verificationID;
      this.autorizarUsuario();

    }).catch((error) => {

      this.finalizarLoading();

      if(error === 'TOO_SHORT'){
        this.mostrarAlerta('Error', 'Por favor ingrese un número de teléfono válido');
      }else{
        this.mostrarAlerta('Error', 'Hubo un error verificando el número de teléfono, por favor inténtelo de nuevo.');
      }
    });
  }

  autorizarUsuario(){

    if (this.idVerificacion) {
        this.router.navigateByUrl('/autorizacion/'+this.idVerificacion);
      } else {
        console.log('idVerificación no definido: ', this.idVerificacion );
      }
  }

  registrarUsuario(){
    this.router.navigateByUrl('/registro');
  }

  mostrarLoading(){
    this.loadingController.create({
      message: 'Verificando...'
    }).then((loading=>{
      this.loadingRef = loading;
      this.loadingRef.present();
    }));
  }

  finalizarLoading(){
    this.loadingRef.dismiss();
  }

  async mostrarAlerta(titulo: string, mensaje: string) {
    const alert = await this.alertController.create({
      header: titulo,
      message: mensaje,
      buttons: ['ACEPTAR']
    });

    await alert.present();
  }

  openPopover(ev: Event) {
    this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    }).then((popover)=>{
        popover.present();
    }).catch((error)=>{
        console.log('Error abriendo el popover');
    });
  }

}