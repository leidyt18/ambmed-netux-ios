import { Component, OnInit } from '@angular/core';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';
import { RepositorioSolicitudService } from '../../services/repositorio-solicitud.service';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';

@Component({
  selector: 'app-confirmacion',
  templateUrl: './confirmacion.page.html',
  styleUrls: ['./confirmacion.page.scss'],
})
export class ConfirmacionPage implements OnInit {

  srcData: any;
  infoAdicional: string;
  imagenAcargar: any;
  tipoDeEmergencia: string;
  loadingRef = null;

  options: CameraOptions = {
      quality: 40,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE
  }

  constructor(
    private camera: Camera,
    public popoverController: PopoverController,
    private repositorioSolicitudService: RepositorioSolicitudService,
    private loadingController: LoadingController,
    private alertController: AlertController
  ) { }

  ngOnInit() {

  }

  getTipoEmergencia(itemSeleccionado){
    this.tipoDeEmergencia = itemSeleccionado.detail.value;
  }

  tomarFoto(){
    this.mostrarLoading();
    this.camera.getPicture(this.options).then((imageData) => {
      this.imagenAcargar = imageData;
      let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.srcData = base64Image;
      this.finalizarLoading();
    }, (err) => {
      console.log('Error tomando la foto');
      console.log(err);
      this.finalizarLoading();
      this.mostrarAlerta('Error', 'Hubo un error tomando la foto.');
    });
  }

  confirmarSolicitud(){
    if(this.repositorioSolicitudService.solicitudAmb){
      this.repositorioSolicitudService.solicitudAmb.additionalInfo = this.infoAdicional;
      this.repositorioSolicitudService.solicitudAmb.emergencyType = this.tipoDeEmergencia;
      if(this.imagenAcargar){
        this.repositorioSolicitudService.registrarSolicitudConFoto(this.imagenAcargar);
      }else{
        this.repositorioSolicitudService.registrarSolicitud();
      }
    }else{
      console.log('El obj repositorioSolicitudService.solicitudAmb no esta definido');
    }
  }

  async mostrarAlerta(titulo: string, mensaje: string) {
    const alert = await this.alertController.create({
      header: titulo,
      message: mensaje,
      buttons: ['ACEPTAR']
    });
    await alert.present();
  }

  mostrarLoading(){
    this.loadingController.create({
      message: 'Cargando foto...'
    }).then((loading=>{
      this.loadingRef = loading;
      this.loadingRef.present();
    }));
  }

  finalizarLoading(){
    this.loadingRef.dismiss();
  }

  openPopover(ev: Event) {
    this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    }).then((popover)=>{
        popover.present();
    }).catch((error)=>{
        console.log('Error abriendo el popover');
    });
  }
}
