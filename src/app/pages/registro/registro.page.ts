import { Component, OnInit } from '@angular/core';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { PopoverController } from '@ionic/angular';
import { PopoverPage } from '../popover/popover.page';
import { AlertController } from '@ionic/angular';
import { RepositorioUsuarioService } from '../../services/repositorio-usuario.service';
import { RepositorioSolicitudService } from '../../services/repositorio-solicitud.service';
import { LoadingController } from '@ionic/angular';
import { UsuarioI } from '../../entities/usuario.interface';

@Component({
  selector: 'app-registro',
  templateUrl: './registro.page.html',
  styleUrls: ['./registro.page.scss'],
})
export class RegistroPage implements OnInit {

  nombre: string;
  identificacion: string;
  numeroTelefono: string;
  loadingRef = null;
  idUsuario: any = null;
  esUsuarioRegistrado: boolean;
  
  constructor(
    private firebaseAuthentication: FirebaseAuthentication,
    private repositorioUsuarioService: RepositorioUsuarioService,
    private repositorioSolicitudService: RepositorioSolicitudService,
    private popoverController: PopoverController,
    private loadingController: LoadingController
  ) { }

  ngOnInit() {
    this.nombre = '';
    this.identificacion = '';
    this.numeroTelefono = '';
    this.idUsuario = '';
    this.esUsuarioRegistrado = false;

    this.mostrarLoading('Consultando información...');

    this.firebaseAuthentication.onAuthStateChanged().subscribe((respUsuario) => {
      if (respUsuario) {
        this.idUsuario = respUsuario.uid;
        this.numeroTelefono = respUsuario.phoneNumber;
        console.log('Respuesta usuario: ', respUsuario);
        this.repositorioUsuarioService.obtenerUsuario(respUsuario.uid).subscribe(respuesta => {
          console.log('Respuesta obtener usuario, se supone usuario registrado ya: ', respuesta);
          if(respuesta && respuesta[0]){
            this.esUsuarioRegistrado = true;
            this.nombre = respuesta[0]+"";
            this.identificacion = respuesta[1]+"";
            this.numeroTelefono = respuesta[3]+"";
          }else{
            console.log('El usuario no está registrado', respuesta);
          }
        });
      } else {
        console.log('El usuario no está autenticado');
      }
    });
  }

  registrarUsuario(){
    this.repositorioSolicitudService.inicializarSolicitudAmb();
    this.repositorioSolicitudService.solicitudAmb.userId = this.idUsuario;
    let usuarioNuevo: UsuarioI = {
      'fullName': this.nombre,
      'identification': this.identificacion,
      'phoneNumber': this.numeroTelefono
    };
    
    if(this.esUsuarioRegistrado){
      this.repositorioUsuarioService.actualizarUsuario(usuarioNuevo, this.idUsuario);
    }else{
      this.repositorioUsuarioService.registrarUsuario(usuarioNuevo);
    }
  }

  mostrarLoading(mensaje: string){
    this.loadingController.create({
      message: mensaje
    }).then(loading => {
      loading.present();
      setTimeout(() => {
        loading.dismiss();
      }, 2000);
    });
  }

  openPopover(ev: Event) {
    this.popoverController.create({
      component: PopoverPage,
      event: ev,
      translucent: true
    }).then((popover)=>{
        popover.present();
    }).catch((error)=>{
        console.log('Error abriendo el popover');
    });
  }

}
