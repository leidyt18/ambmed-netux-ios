import { Injectable } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { UsuarioI } from '../entities/usuario.interface';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RepositorioUsuarioService {

  private userCollection: AngularFireList<UsuarioI>;
	private user: Observable<UsuarioI[]>;

  constructor(private db:AngularFireDatabase) {	
		this.userCollection = this.db.list('/users');
	}
	
	obtenerUsuario(id: string){
		console.log('Se va a buscar al usuario ' + '/users/'+id);
		this.userCollection = this.db.list('/users/'+id);
		this.user = this.userCollection.valueChanges();
		return this.user;
	}
	
	actualizarUsuario(usuario: UsuarioI, id: string){
		console.log('Se va a actualizarUsuario', usuario);
		this.userCollection = this.db.list('/users');
		this.userCollection.update(id, usuario);
		console.log('Se actualiza el usuario.');
	}
	
	registrarUsuario(usuario: UsuarioI){
		console.log('Se va a registrarUsuario', usuario);
		this.userCollection = this.db.list('/users');
		this.userCollection.push(usuario);
		console.log('Se registra el usuario.');
	}
  
}
