import { TestBed } from '@angular/core/testing';

import { RepositorioUsuarioService } from './repositorio-usuario.service';

describe('RepositorioUsuarioService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RepositorioUsuarioService = TestBed.get(RepositorioUsuarioService);
    expect(service).toBeTruthy();
  });
});
