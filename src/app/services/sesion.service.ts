import { Injectable } from '@angular/core';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';

@Injectable({
  providedIn: 'root'
})
export class SesionService {

  usuarioActivo: null;

  constructor(public firebaseAuthentication: FirebaseAuthentication) { }
  
  cerrarSesion(){
    this.firebaseAuthentication.signOut().then((resp) => {
      console.log('Se cierra la sesion', resp);
    });
  }
}
