import { Injectable } from '@angular/core';
import { SolicitudI } from '../entities/solicitud.interface';
import { AngularFireList, AngularFireDatabase } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import { Router } from '@angular/router';
import { LoadingController } from '@ionic/angular';
import { AlertController } from '@ionic/angular';
import { SesionService } from '../services/sesion.service';

@Injectable({
  providedIn: 'root'
})
export class RepositorioSolicitudService {

	private req_ambulanceCollection: AngularFireList<SolicitudI>;
	private urlFoto: string;

	public solicitudAmb: SolicitudI;

	constructor(
		private db: AngularFireDatabase, 
		private afStorage: AngularFireStorage, 
		private router: Router, 
		private loadingController: LoadingController,
		private alertController: AlertController,
		public sesionService: SesionService) 
	{
		this.req_ambulanceCollection = db.list('/req_ambulances');
	}

	inicializarSolicitudAmb(){
		this.solicitudAmb = {
			'additionalImageURL': '',
			'additionalInfo': '',
			'address': '',
			'emergencyType': '',
			'latitude': 0,
			'longitude': 0,
			'userId': ''
		}
	}
	
	registrarSolicitud(){
		this.req_ambulanceCollection.push(this.solicitudAmb);
		this.sesionService.cerrarSesion();
		this.mostrarAlerta('Listo!', 'Solicitud registrada con éxito.');
		this.router.navigateByUrl('/inicio');
	}
	
	registrarSolicitudConFoto(imagenBase64: any){
		this.loadingController.create({
      message: 'Registrando solicitud de ambulancia...'
    }).then(loading => {

			loading.present();
			
			let fecha = new Date();
			let dd = String(fecha.getDate()).padStart(2, '0');
			let mm = String(fecha.getMonth() + 1).padStart(2, '0');
			let yyyy = fecha.getFullYear();
			let hoy = yyyy + mm + dd;

			let name = 'additional_photo_' + hoy + '_' + fecha.getTime() + '.jpeg';

			let metadata = {
				contentType: 'image/jpeg'
			};

			try {
				this.afStorage.ref('/ambulances_additional_images/' + name).putString(imagenBase64, 'base64', metadata).then(respCarga => {
					loading.dismiss();
					this.afStorage.ref('/ambulances_additional_images/' + name).getDownloadURL().subscribe(url => {
						this.solicitudAmb.additionalImageURL = url;
						this.registrarSolicitud();
					});
				});
			} catch (error) {
				loading.dismiss();
				this.mostrarAlerta('Error', 'No se pudo cargar la foto, la solicitud de ambulancia será registrada sin ella.');
				this.registrarSolicitud();
			}
			
		});
	}

	async mostrarAlerta(titulo: string, mensaje: string) {
    const alert = await this.alertController.create({
      header: titulo,
      message: mensaje,
      buttons: ['ACEPTAR']
    });
    await alert.present();
  }
  
}