import { TestBed } from '@angular/core/testing';

import { RepositorioSolicitudService } from './repositorio-solicitud.service';

describe('RepositorioSolicitudService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: RepositorioSolicitudService = TestBed.get(RepositorioSolicitudService);
    expect(service).toBeTruthy();
  });
});
