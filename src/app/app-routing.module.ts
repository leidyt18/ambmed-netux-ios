import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: '', redirectTo: 'inicio', pathMatch: 'full' },
  { path: 'inicio', loadChildren: './pages/inicio/inicio.module#InicioPageModule' },
  { path: 'autenticacion', loadChildren: './pages/autenticacion/autenticacion.module#AutenticacionPageModule' },
  { path: 'autorizacion/:idVerificacion', loadChildren: './pages/autorizacion/autorizacion.module#AutorizacionPageModule' },
  { path: 'registro', loadChildren: './pages/registro/registro.module#RegistroPageModule' },
  { path: 'localizacion', loadChildren: './pages/localizacion/localizacion.module#LocalizacionPageModule' },
  { path: 'confirmacion', loadChildren: './pages/confirmacion/confirmacion.module#ConfirmacionPageModule' },
  { path: 'popover', loadChildren: './pages/popover/popover.module#PopoverPageModule' },
  { path: 'modal-imagen', loadChildren: './pages/modal-imagen/modal-imagen.module#ModalImagenPageModule' },
];

@NgModule({
  
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
  
})
export class AppRoutingModule { }
