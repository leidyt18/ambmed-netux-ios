import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';
import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PopoverPage } from './pages/popover/popover.page';
import { FirebaseAuthentication } from '@ionic-native/firebase-authentication/ngx';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import { GoogleMaps } from '@ionic-native/google-maps';
import { FormsModule } from '@angular/forms';
import { Camera } from '@ionic-native/camera/ngx';
import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireStorage } from 'angularfire2/storage';
import { ModalImagenPageModule } from '../app/pages/modal-imagen/modal-imagen.module';

const firebase = {
  production: false,
  firebaseConfig: {
  apiKey: "AIzaSyDMYPD1dzh11PTGefk5cYz_213BMIKH-7o",
  authDomain: "ambulances-med.firebaseapp.com",
  databaseURL: "https://ambulances-med.firebaseio.com",
  projectId: "ambulances-med",
  storageBucket: "ambulances-med.appspot.com",
  messagingSenderId: "470730114110"
  }
};


@NgModule({
  declarations: [AppComponent, PopoverPage],
  entryComponents: [PopoverPage],
  imports: [BrowserModule, 
            IonicModule.forRoot(),
            AppRoutingModule,
            AngularFireModule.initializeApp(firebase.firebaseConfig),
            FormsModule,
            AngularFirestoreModule,
            AngularFireDatabaseModule,
            ModalImagenPageModule
          ],
  providers: [
    StatusBar,
    SplashScreen,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    GoogleMaps,
    FirebaseAuthentication,
    Camera,
    AngularFireStorage
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
