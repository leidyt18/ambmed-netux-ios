export interface SolicitudI {

	additionalImageURL: string;
	additionalInfo: string;
	address: string;
	emergencyType: string;
	latitude: number;
	longitude: number;
	userId: string;
	
}