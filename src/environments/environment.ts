// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebase: {
  apiKey: "AIzaSyDMYPD1dzh11PTGefk5cYz_213BMIKH-7o",
  authDomain: "ambulances-med.firebaseapp.com",
  databaseURL: "https://ambulances-med.firebaseio.com",
  projectId: "ambulances-med",
  storageBucket: "ambulances-med.appspot.com",
  messagingSenderId: "470730114110"
  }
};

export const firebaseConfig = {
  apiKey: "AIzaSyDMYPD1dzh11PTGefk5cYz_213BMIKH-7o",
  authDomain: "ambulances-med.firebaseapp.com",
  databaseURL: "https://ambulances-med.firebaseio.com",
  projectId: "ambulances-med",
  storageBucket: "ambulances-med.appspot.com",
  messagingSenderId: "470730114110"
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
